docker-compose-medialib-mediaserver
 
=====================
docker-compose definition for deployment of mediaserver for medialibrary


Docker-compose
--------------

contains : 
 - Application container based on apache with php
 - NFS client container
 - Traefik

It is started using Foreman or ansible which installs docker and docker-compose and  creates:
 - .env file



Result
------

Working mediaserver 

Limitations
-----------
This has been tested.
